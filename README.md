# mtDepths - mtDNA Sequencing Depths

This Nextflow pipeline will retrieve the sequencing depths for each of the mitochondrias 16,569 base-pairs for each of the samples provided in a parameters file.

## Running the Pipeline

### 1. Cloning Repository

Clone the repository and cd into the directory using the following commands:

```shell
git clone https://git.ecdf.ed.ac.uk/s2078878/mtDepths.git
cd mtDepths
```

### 2. Create Environment

To create and activate a conda environment with all the necessary packages, run the following commands:

```shell
conda env create --file env.yml
conda activate mtDepths
```

### 3. Make Support Files

#### Matched-Samples
The Nextflow pipeline works from a .csv file containing the main parameters needed. The csv file should have the following format and column names:

| row | sampleID | bam                           | index                         |
|:----|:---------|:------------------------------|:------------------------------|
| 1   | sample1  | path/to/sample1/alignment.bam | path/to/sample1/index.bam.bai |
| 2   | sample2  | path/to/sample2/alignment.bam | path/to/sample2/index.bam.bai |
| 3   | sample3  | path/to/sample3/alignment.bam | path/to/sample3/index.bam.bai |
| 4   | sample4  | path/to/sample4/alignment.bam | path/to/sample4/index.bam.bai |

Alignment and index files can be for the entire genome, there is no need to subset to only the mitochondrial DNA.

### 4. Run mtDepths

Everything should now be ready to run mtDepths. A typical command will look something similar to the one below, substituting in the path to your own parameter file. Take additional note of setting the `--chr` parameter determining how the mitochondrial genome is denoted in the alignment files. 

```shell
nextflow run Depths.nf \
  --parameters path/to/parameters/file.csv \ # parameters file (see step 3)
  --outdir path/to/out/directory/ \ # where should the results folder be created? default: run directory
  --chr (chrM / MT) # how is the mitochondrial genome denoted? default: chrM
```

### 5. Outputs

A single output file will be generated in the results directory. This will contain the read depths for each of the mitochondrias 16,569 base-pairs for each of the samples contained within the input file. 

## Notes

Please be aware that this pipelines `nextflow.config` file has been configured to run on the University of Edinburgh's Eddie HPC Cluster. This may need to be reconfigured to run on other computing systems. 







