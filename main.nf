#!/usr/bin/env nextflow

log.info """\
         =============================================================================
         M T D E P T H :   M I T O C H O N D R I A L   S E Q U E N C I N G   D E P T H
         =============================================================================
         parameters_file:   ${params.parameters}
         out_directory:     ${params.outdir}
         =============================================================================
         """
         .stripIndent()

/*
 * Input parameters validation
 */
if (params.parameters){
    
    parameters_file = file(params.parameters)

    Channel
        .fromPath(params.parameters)
        .splitCsv(header:true)
        .map{ row -> tuple(row.row, row.sampleID, row.bam, row.index)}
        .set{ parameters_ch }
    
    if( !parameters_file.exists() ) exit 1, "ERROR: Parameters file doesn't exist: ${parameters_file}"

} else {
    
    exit 1, "ERROR: Missing parameters file. Please set a parameters file using the --parameters flag."

}

/*
 * Extract only the mitochondria reads from the bam files
 */
process chrm {
    tag "Pulling chrM from bam file"

    input:
    set row, sampleID, path(bam), path(index) from parameters_ch

    output:
    set sampleID, path("${sampleID}.bam"), path("${sampleID}.bam.bai") into mitochondria_ch

    script:
    """
    samtools view -h -b ${bam} chrM MT M chrMT > ${sampleID}.bam
    samtools index ${sampleID}.bam
    """
}

/*
 * Get read depths
 */
process depths {
    tag "Getting sample read depths"

    input:
    set sampleID, path(bam), path(index) from mitochondria_ch

    output:
    path("${sampleID}.txt") into depths_ch

    script:
    """
    CHRNAME=(\$(samtools idxstats ${bam} | sort -k3,3 | tail -1 | cut -f1))
    samtools depth -d 1000000 -r \$CHRNAME -aa ${bam} > ${sampleID}.txt
    """
}

/*
 * Merge read depths
 */
process merge {
    tag "Merging sample read depths"
    publishDir "${params.outdir}/results" , mode:'copy'

    input:
    path(depths) from depths_ch.collect()

    output:
    path("depths.txt") into results_ch

    script:
    """ 
    cp $baseDir/scripts/merge.R .
    Rscript merge.R ${depths}
    """
}

workflow.onComplete {
	log.info ( workflow.success ? "\nDone! All depths calculated.\n" : "\nOops .. something went wrong.\n" )
}